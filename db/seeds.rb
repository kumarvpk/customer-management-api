# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#tokrn: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjo2LCJleHAiOjE1NTA4NDQxMDV9.tonuPhZ1S16sMnQNzGwYg9DcA9dwiPPyb0RouIzDMok

user = User.create
puts JsonWebToken.encode(user: user.id)
puts 'This is your authenticat_api token.'
