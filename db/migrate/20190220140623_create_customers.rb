class CreateCustomers < ActiveRecord::Migration[5.1]
  def change
    create_table :customers do |t|
      t.string :name
      t.belongs_to :address, index: true
      t.belongs_to :user, index: true
      t.timestamps
    end
  end
end
