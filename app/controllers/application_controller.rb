class ApplicationController < ActionController::API
  before_action :verify_authenticity_token
  rescue_from Exception do |exception|
    if exception.is_a? JsonWebToken::ApiAuthException
      render_401
    end
  end

  def success_200(response = {})
    render json:
    {
      status: 200,
      message: 'Successfully response created.'
    }.merge(response).to_json
  end

  def unsuccess_200(response = {})
    render json:
    {
      status: 200,
      message: 'Something went wrong.'
    }.merge(response).to_json
  end

  def render_401
    render nothing: true, status: 401
  end

  def render_404
    render nothing: true, status: 404
  end

  def render_500
    render nothing: true, status: 500
  end

  def verify_authenticity_token
    @user = JsonWebToken.decode request.headers[:token]
  end
end
