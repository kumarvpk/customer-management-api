class Api::V1::CustomersController < ApplicationController
  before_action :get_customer, only: [:show, :destroy, :update]


  def create
    begin
      customer = Customer.create!(customer_params)
      address = Address.create!(address_params)
      customer.update!(address: address)
      success_200(customer: customer.to_json(include: [:address]))
    rescue Exception => e
      unsuccess_200(customer: { error: e.message })
    end
  end

  def show
    if @customer
      success_200(customer: @customer.to_json(include: [:address]))
    else
      render_404
    end
  end

  def update
    begin
      if @customer
        @customer.update!(customer_params)
        @customer.address.update!(address_params)
        success_200(customer: @customer.to_json(include: [:address]))
      else
        render_404
      end
    rescue Exception=>e
       unsuccess_200(customer: { error: e.message })
    end
  end

  def details
    filters
    success_200(customer: @customers.to_json(include: [:address]))
  end

  def destroy
    if @customer
      if @customer.destroy
        render json: {
          status: 200,
          message: 'Successfully deleted.'
        }.merge(response).to_json
      else
        unsuccess_200(customer: { error: @customer.errors.full_messages })
      end
    else
      render_404
    end
  end

  private
  def customer_params
    params[:user_id] = @user.id
    params.permit(:name, :user_id)
  end

  def address_params
    params.permit(:street, :city, :zipcode)
  end

  def get_customer
    @customer = Customer.find_by_id(params[:id])
  end

  def filters
    @customers = @user.customers.includes(:address)
    @customers = @customers.by_name(params[:name]) if params[:name]
    @customers = @customers.by_address_street(params[:street]) if params[:street]
    @customers
  end
end
