class Customer < ApplicationRecord
  #validations
  validates :name, presence: true, uniqueness: true

  belongs_to :address, dependent: :destroy, optional: true
  belongs_to :user

  scope :by_name, ->(name){ where("name LIKE ?", "%#{name}%") }
  scope :by_address_street, ->(street){ joins(:address).where("addresses.street LIKE ?", "%#{street}%")  }
end
