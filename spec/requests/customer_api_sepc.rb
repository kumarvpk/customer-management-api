require 'rails_helper'

RSpec.describe "customers api" , type: :request do

  describe "#Customer API" do
    before do
      @user = create(:user)
    end

    let(:user) { @user }
    let(:token) { JsonWebToken.encode({user: @user.id })}
    let(:customer) { user.customers.first }
    let(:address) { customer.address }

    describe "GET #show" do
      context '#Failur' do
        it 'return 401 error without token' do
          get '/api/v1/customer', params: { id: customer.id }
          expect(response.status).to eq(401)
        end

        it 'return 404 error without id' do
          get '/api/v1/customer', headers: {'token' => token}, params: { id: nil }
          expect(response.status).to eq(404)
        end

        it 'return 404 error with invalid id' do
          get '/api/v1/customer', headers: {'token' => token}, params: { id: 4435 }
          expect(response.status).to eq(404)
        end
      end

      context '#Success' do
        before do
          get '/api/v1/customer', headers: {'token' => token}, params: { id: customer.id }
        end

        it 'return 200 with valid id' do
          expect(response.status).to eq(200)
        end
      end
    end

    describe "POST #crate" do
      context '#Failur' do
        it 'return 401 error without token' do
          get '/api/v1/customer'
          expect(response.status).to eq(401)
        end
      end

      context '#Success' do
        it 'Should create user and associated address with valid params' do
          expect(Customer.count).to eq(1)
          expect(Address.count).to eq(1)

          post '/api/v1/customer', headers: {'token': token}, params: customer_valid_params
          expect(response.status).to eq(200)
          expect(Customer.count).to eq(2)
          expect(Address.count).to eq(2)
          expect(Customer.last.address.city).to eq('Test city 2')
        end
      end
    end

    describe "UPDATE #patch" do
      context '#Failur' do
        it 'return 401 error without token' do
          get '/api/v1/customer'
          expect(response.status).to eq(401)
        end

        it 'return 404 error with invalid id' do
          get '/api/v1/customer', headers: {'token' => token}, params: { id: nil }
          expect(response.status).to eq(404)
        end
      end

      context '#Success' do
        it 'Should update user and addres attributes' do
          patch '/api/v1/customer', headers: {'token': token}, params: customer_valid_params.merge(id: customer.id)

          expect(response.status).to eq(200)
          expect(Customer.count).to eq(1)
          expect(Customer.first.name).to eq(customer_valid_params[:name])
          expect(Customer.first.address.city).to eq(customer_valid_params[:city])
        end
      end
    end

     describe "Destroy #delete" do
      context '#Failur' do
        it 'return 401 error without token' do
          get '/api/v1/customer'
          expect(response.status).to eq(401)
        end

        it 'return 404 error with invalid id' do
          get '/api/v1/customer', headers: {'token' => token}, params: { id: nil }
          expect(response.status).to eq(404)
        end
      end

      context '#Success' do
        it 'Should delete customer and associated address' do
          delete '/api/v1/customer', headers: {'token': token}, params: { id: customer.id }
          expect(response.status).to eq(200)
          expect(Customer.count).to eq(0)
        end
      end
    end

    describe "details #get" do
      context '#Failur' do
        it 'return 401 error without token' do
          get '/api/v1/customer/details'
          expect(response.status).to eq(401)
        end
      end

      context '#Success' do
        it 'Should fetch list of all customers' do
          get '/api/v1/customer/details', headers: {'token' => token}
          expect(response.status).to eq(200)
        end
      end
    end
  end

  private
  def customer_valid_params
    {
      name: 'Test user 2',
      city: 'Test city 2',
      street: 'Test street 2',
      zipcode: '222222'
    }
  end
end
