FactoryBot.define do
  factory :user do
    after :create do |user|
      address = create :address
      create(:customer, address:  address, user: user)
    end
  end
end
