FactoryBot.define do
  factory :customer do
    name { 'Test user' }
    address { nil }
    user { nil }
  end
end
