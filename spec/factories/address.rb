FactoryBot.define do
  factory :address do
    street { 'Test street first' }
    city { 'Test city first' }
    zipcode { '11111' }
  end
end
