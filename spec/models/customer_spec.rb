require 'rails_helper'
RSpec.describe Customer, type: :model do
  it { should validate_presence_of(:name) }
  it { should validate_uniqueness_of(:name) }

  it { should belong_to(:user) }
  it { should belong_to(:address).optional }
end
